import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent {
  error: string = '';
  loading: boolean = false;
  constructor(
    private router: Router
  ) { }

  onSubmit(formulary:any){
    if(formulary.form.value.email == environment.email && formulary.form.value.password == environment.password){
      this.router.navigate(['home']);
    }else{
      this.error = 'Verifique seus dados!';
    }
  }

}
