import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HabitsHomeComponent } from './habits-home.component';

describe('HabitsHomeComponent', () => {
  let component: HabitsHomeComponent;
  let fixture: ComponentFixture<HabitsHomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HabitsHomeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HabitsHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
