import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { HabitsFormComponent } from './components/forms/habits-form/habits-form.component';
import { LoginComponent } from './components/login/login.component';
import { LoginFormComponent } from './components/forms/login-form/login-form.component';
import { HabitsHomeComponent } from './components/habits-home/habits-home.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { CardComponent } from './components/card/card.component'

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HabitsFormComponent,
    LoginComponent,
    LoginFormComponent,
    HabitsHomeComponent,
    NavbarComponent,
    CardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
